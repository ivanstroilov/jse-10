package ru.t1.stroilov.tm.model;

import ru.t1.stroilov.tm.enumerated.Status;

import java.util.UUID;

public class Project {

    private String id = UUID.randomUUID().toString();
    private String name = "";
    private String description = "";

    private Status status = Status.NOT_STARTED;

    public Project() {
    }

    public Project(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public Project(final String name, final Status status) {
        this.name = name;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (description != null && !description.isEmpty()) result += " : " + description;
        if (status != null) result += " : " + Status.toName(status);
        return result;
    }
}
